clear all; clc

run('setSimulationVariables')
disp('setSimulationVariables');

isOutputFolderExist = exist( 'myOutputDir'); %Check if the output folder alreadey exists 
isMyBasebandCenvTimeDomainDirExist = exist( 'myBasebandCenvTimeDomainDir'); %Check if the output folder alreadey exists 

%0) Create directories in case they do not exist
 if(isOutputFolderExist ~= 1)
     mkdir(myOutputDir);
   
 end;
 if(isMyBasebandCenvTimeDomainDirExist ~= 1)
     mkdir(myBasebandCenvTimeDomainDir);
 end;
  



%1) create baseband complex envelopes. This can be done in SystemVue,
%for example. Write files into a folder such as "complexBasebandEnvTimeDomain".
disp('1) create complex envelopes')
disp('rat_generateAllAMSignals')
rat_generateAllAMSignals
pause
disp('rat_generateAllDSBSCSignals')
rat_generateAllDSBSCSignals
disp('rat_generateAllQAMSignals')
rat_generateAllQAMSignals

%2) describe each scenario and create associated files:
%setupDescription.m, testFilesList.txt and trainFilesList.txt
%inside each scenario directory
disp('2) Manually describe each scenario and create associated files');
disp('Do not forget to create setupDescription.m, testFilesList.txt and trainFilesList.txt')

%3) Create multiplex signals
disp('3) Create multiplex IF complex envelopes in time domain')
disp('rat_generateTrainTestSequences')
rat_generateTrainTestSequences

%4) Filter and convert to frequency domain
%- create the list you specied in
%listOfIFComplexEnvelopes = [myRATRootDir filesep 'filesToHavePSDCalculated.txt'];
%- convert to PSDs:
disp('4) Filter and convert to frequency domain')
disp('rat_convertAllToPSDs')
rat_convertAllToPSDs

%5) Extract features and labels
disp('5) Extract features and labels')
disp('rat_generateAllFeaturesAndLabels')
rat_generateAllFeaturesAndLabels