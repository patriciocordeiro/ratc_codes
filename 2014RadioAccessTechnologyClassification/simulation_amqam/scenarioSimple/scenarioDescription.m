%minFreq = 100e3; %Frequency range: min to max
%maxFreq = 200e3; 
%This scenario uses a scenario with 1 AM and 1 QAM
%In RF their frequencies are minFreq+centerFrequencies
%Files with the complex envelopes in baseband
%Files are little-endian float values, real and imaginary
%interleaved: xreal[0],ximag[0],xreal[1],ximag[1],xreal[2],ximag[2]..
centerFrequencies=[20 60]*1e3; %center frequencies
ratBandwidths=[10 7.5]*1e3;
%classLabels = {'AM','QAM'};
labels=[1 2]; %AM QAM
