%minFreq = 100e3; %Frequency range: min to max
%maxFreq = 200e3; 
%This scenario uses a scenario with 2 AM and 2 QAM and 1 DSB-SC. 
%In RF their frequencies are minFreq+centerFrequencies
%100 + [25 35 45 70 85]*1e3
%Files with the complex envelopes in baseband
%Files are little-endian float values, real and imaginary
%interleaved: xreal[0],ximag[0],xreal[1],ximag[1],xreal[2],ximag[2]..
centerFrequencies=[10 25 45 70 85]*1e3; %center frequencies
ratBandwidths=[10 7.5 10 7.5 15]*1e3;
%classLabels = {'AM','QAM','DSBSC'};
labels=[1 2 1 2 3]; %AM QAM AM QAM DSB-SC
