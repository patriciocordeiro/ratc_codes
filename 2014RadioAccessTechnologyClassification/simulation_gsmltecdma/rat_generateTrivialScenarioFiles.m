%I will make automatic the process of creating the scenario files.
run('setSimulationVariables')

mkdir([myRATRootDir 'scenarioTrivial'])

fp2=fopen([myRATRootDir 'scenarioTrivial' filesep 'scenarioDescription.m'],'w');
fprintf(fp2,'%s\n',['labels=[1 3 2]; %GSM, LTE and then WCDMA, from:']);
fprintf(fp2,'%s\n',['%classLabels = {''GSM'',''WCDMA'',''LTE'',''noRAT''};']);
%recall that Fs=8e6 and IF bandwidth is counted from -Fs/2 to Fs/2
fprintf(fp2,'%s\n',['centerFrequencies=[-3000 100 2900]*1e3; %center frequencies']);
fprintf(fp2,'%s\n',['ratBandwidths=[200 1400 200]*1e3;']);
fclose(fp2);

fp2=fopen([myRATRootDir 'scenarioTrivial' filesep 'trainScriptsPerScenarioList.txt'],'w');
for j=1:numTrain
    fp=fopen([myRATRootDir 'scenarioTrivial' filesep ...
        'gsmltecdmSignals_train_' num2str(j) '.m'],'w');
    fprintf(fp,'%s\n',['inputPath = myBasebandCenvTimeDomainDir;']);
    fprintf(fp,'%s\n',['outputFileName = [myOutputDir ''gsmltecdma_' num2str(j) '''];']);
    
    fprintf(fp,'%s\n',['complexEnvelopes{1}=[inputPath ''gsmFs8MHzBw200kHz_' num2str(j) '''];']);
    fprintf(fp,'%s\n',['complexEnvelopes{2}=[inputPath ''lteFs8MHzBw1400kHz_' num2str(j) '''];']);
    fprintf(fp,'%s\n',['complexEnvelopes{3}=[inputPath ''wcdmaFs8MHzBw200kHz_' num2str(j) '''];']);
    fclose(fp);
    
    fprintf(fp2,'%s\n',['gsmltecdmSignals_train_' num2str(j) '.m']);
end
fclose(fp2);

%% Test
fp2=fopen([myRATRootDir 'scenarioTrivial' filesep 'testScriptsPerScenarioList.txt'],'w');
for j=numTrain+1:numTrain+numTest;
    fp=fopen([myRATRootDir 'scenarioTrivial' filesep ...
        'gsmltecdmSignals_test_' num2str(j) '.m'],'w');
    fprintf(fp,'%s\n',['inputPath = myBasebandCenvTimeDomainDir;']);
    fprintf(fp,'%s\n',['outputFileName = [myOutputDir ''gsmltecdma_' num2str(j) '''];']);
    
    fprintf(fp,'%s\n',['complexEnvelopes{1}=[inputPath ''gsmFs8MHzBw200kHz_' num2str(j) '''];']);
    fprintf(fp,'%s\n',['complexEnvelopes{2}=[inputPath ''lteFs8MHzBw1400kHz_' num2str(j) '''];']);
    fprintf(fp,'%s\n',['complexEnvelopes{3}=[inputPath ''wcdmaFs8MHzBw200kHz_' num2str(j) '''];']);
    fclose(fp);
    
    fprintf(fp2,'%s\n',['gsmltecdmSignals_test_' num2str(j) '.m']);
end
fclose(fp2);

%% Validation
fp2=fopen([myRATRootDir 'scenarioTrivial' filesep 'validationScriptsPerScenarioList.txt'],'w');
for j=numTrain+numTest:numTrain+numTest+numVali
    fp=fopen([myRATRootDir 'scenarioTrivial' filesep ...
        'gsmltecdmSignals_validation_' num2str(j) '.m'],'w');
    fprintf(fp,'%s\n',['inputPath = myBasebandCenvTimeDomainDir;']);
    fprintf(fp,'%s\n',['outputFileName = [myOutputDir ''gsmltecdma_' num2str(j) '''];']);
    
    fprintf(fp,'%s\n',['complexEnvelopes{1}=[inputPath ''gsmFs8MHzBw200kHz_' num2str(j) '''];']);
    fprintf(fp,'%s\n',['complexEnvelopes{2}=[inputPath ''lteFs8MHzBw1400kHz_' num2str(j) '''];']);
    fprintf(fp,'%s\n',['complexEnvelopes{3}=[inputPath ''wcdmaFs8MHzBw200kHz_' num2str(j) '''];']);
    fclose(fp);
    
    fprintf(fp2,'%s\n',['gsmltecdmSignals_validation_' num2str(j) '.m']);
end
fclose(fp2);
