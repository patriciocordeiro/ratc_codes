clear all

run('setSimulationVariables')
disp('setSimulationVariables');

%0) Create directories in case they do not exist
mkdir(myOutputDir);
mkdir(myBasebandCenvTimeDomainDir);

%1) create baseband complex envelopes. This can be done in SystemVue,
%for example. Write files into a folder such as "complexBasebandEnvTimeDomain".
disp('1) create baseband complex envelopes')
disp('rat_generateAllComplexEnvelopesInBinary')
rat_generateAllComplexEnvelopesInBinary

%2) describe each scenario and create associated files:
%setupDescription.m, testFilesList.txt and trainFilesList.txt
%inside each scenario directory
disp('2) Automatically describe each scenario and create associated files');
disp('rat_generateTrivialScenarioFiles')
rat_generateTrivialScenarioFiles

%3) Create multiplex signals
disp('3) Create multiplex IF complex envelopes in time domain')
disp('rat_generateTrainTestSequences')
rat_generateTrainTestSequences

%4) Filter and convert to frequency domain
%- create the list you specied in
%listOfIFComplexEnvelopes = [myRATRootDir filesep 'filesToHavePSDCalculated.txt'];
%- convert to PSDs:
disp('4) Filter and convert to frequency domain')
disp('rat_convertAllToPSDs')
rat_convertAllToPSDs

%5) Extract features and labels
disp('5) Extract features and labels')
disp('rat_generateAllFeaturesAndLabels')
rat_generateAllFeaturesAndLabels