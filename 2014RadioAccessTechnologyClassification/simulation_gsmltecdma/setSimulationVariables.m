%% First part: things you *must* change or at least check
% Specify your main directories
%NOTE: end all directories with filesep ('/' or '\' depending on
%if you are using Windows or Linux) otherwise concatenation is wrong

% Choose the user (a convenience)
%user='Patricio';
%user='Vitoria';
user='Aldebaro';
%Recall that directories need to end with filesep
switch user
    case 'Patricio' %Windows user
        myModuRatCDir = 'C:\svns\laps\telecom\2014RadioAccessTechnologyClassification\';
        pathToAldebarosCode = 'C:\svns\laps\latex\dslbook\'; %modify to match your system
    case 'Aldebaro' %Windows (argh) user
        myModuRatCDir = 'C:\svns\laps\telecom\2014RadioAccessTechnologyClassification\';
        pathToAldebarosCode = 'C:\svns\laps\latex\dslbook\'; %modify to match your system
    case 'Vitoria' %Linux user
        myModuRatCDir = '/home/v/2014RadioAccessTechnologyClassification/';
        pathToAldebarosCode = '/home/v/dslbook/'; %modify to match your system
end

%Specify directories:
myRATRootDir = [myModuRatCDir 'simulation_gsmltecdma' filesep];
myBasebandCenvTimeDomainDir = [myRATRootDir 'complexBasebandEnvTimeDomain' filesep];

%Specify all scenarios of this simulation
directoriesWithScenarios{1}='scenarioTrivial';

% Technical values
%Always include 'noRAT' as the last class even if you are not going
%to use it by shouldSkipNoRATExamples!
classLabels = {'GSM','LTE','WCDMA','noRAT'};
%In case you do not want to include the "noRAT" in ARFF file, enable:
shouldSkipNoRATExamples = 1; %use 1 to enable or 0 otherwise

%Patricio did not inform Fs. Will assume something arbitrary
%minRFFreq = 100e3; %RF frequency range: min to max
%maxRFFreq = 200e3;
Fs = 8e6; %sampling frequency in Hz

%Because digital signal processing is used, the IF bandwidth is
%in fact mapped to frequencies in radians. Hence, using [-Fs/2, Fs/2]
%or [0, Fs] should lead to the same result.
%The convention is:
minIFFreq = -Fs/2; %IF frequency range: min to max
maxIFFreq = Fs/2;
minRATSeparationInHz = 50e3; %minimum separation between RATs, taking
%in account their center frequencies and bandwidth

%Among all RATs, define min and max, to be used for checking
minBW = 200e3; %minimum BW in Hz
maxBW = 1400e3; %maximum BW in Hz

numberOfPSDBlocks = 8; %number of blocks for Welch's
analysisBW = 20e3; %for each of this "small" band, a labeled example is generated
minNumberOfBinsWithinAnalysisBW=16; %number of FFT bins for the
%analysis bandwidth. Recall that for each "analysis" band, a labeled example
%is generated

N0_dBmHz = -100; %unilateral PSD in dBm/Hz

% Front ends
allFrontEnds{1}='rat_frontendPSD'; %simply calculates PSD
allFrontEnds{2}='rat_frontendDCT'; %takes the DCT of the PSD
allFrontEnds{3}='rat_frontendBWAndOtherMeasurements'; %measures BW
numFeatures{1}=20; %number of features of each front end
numFeatures{2}=15;
numFeatures{3}=3;
frontEndIndex = 1; %index of chosen front end

% General parameters to control simulation
showPlots=0; %use 1 to show plots

% Design prototype filter
%h=butter %TO-DO

%IMPORTANT: each time you change the "front end", the number of
%files is different, so change below accordingly
%Case 1:
%N=57 files of each RAT
%numTrain=20; numTest=20; numVali=17;
%Case 2:
%N=12 files of each RAT
numTrain=6; numTest=4; numVali=2;
%Case 3:
%N=6 files of each RAT
numTrain=4; numTest=1; numVali=1;


%% Second part: things you do not need to change

myOutputDir = [myRATRootDir 'output' filesep];

%define names for lists and files
nameListWithTrainScriptsPerScenario{1}='trainScriptsPerScenarioList.txt';
nameListWithTrainScriptsPerScenario{2}='testScriptsPerScenarioList.txt';
nameListWithTrainScriptsPerScenario{3}='validationScriptsPerScenarioList.txt';
listWithPSDAndAssociatedLabels{1}='trainPSDsAndScenariosList.txt';
listWithPSDAndAssociatedLabels{2}='testPSDsAndScenariosList.txt';
listWithPSDAndAssociatedLabels{3}='validationPSDsAndScenariosList.txt';
listOfIFComplexEnvelopes = 'filesToHavePSDCalculated.txt';
outputWekaFile{1}='amqam_train.arff';
outputWekaFile{2}='amqam_test.arff';
outputWekaFile{3}='amqam_validation.arff';

%% Extensions
basebandEnvelopesFileExtension = 'cenvbb';
intermediateFreqEnvelopesFileExtension = 'cenvif';
intermediateFreqPSDsFileExtension = 'psdif';

%% Third part: Pre-calculated and useful variables
if 1 %add folder with functions to path
    P=path;
    path(P,[pathToAldebarosCode 'Code' filesep 'MatlabOctaveFunctions']);
    addpath([myModuRatCDir 'matlabCode'],'-end');
end

df = analysisBW/minNumberOfBinsWithinAnalysisBW; %FFT frequency spacing
M = ceil(Fs/df);
b=nextpow2(M);
Nfft=2^b; %choose a power of 2 for FFT-length
pwelchOverlap=floor(Nfft/2);

BW_IF = maxIFFreq - minIFFreq;
frequencyShift = analysisBW/2; %shift between examples in PSD domain

if analysisBW > minBW/2
    warning('analysisBW may be too large: analysisBW > minBW/2');
end

%S=pwelch(x,hamming(M),floor(M/2),Nfft,Fs,'twosided'); %overlap is M/2

numSamples = numberOfPSDBlocks*pwelchOverlap+Nfft;
%numSamples = Fs*totalDuration;

totalDuration=numSamples/Fs; %seconds

numClasses = length(classLabels);
if shouldSkipNoRATExamples==1
    numClasses = numClasses-1;
end

N0 = 1e-3 * 10^(0.1*N0_dBmHz); %N0 in Watts/Hz
noisePowerPerDimension = N0/2 * Fs;

%TODO I think that in this case, BW_IF is always equal to Fs!!!
if BW_IF > Fs
    error('BW_IF > Fs. Increase Fs!')
end

numScenarios=length(directoriesWithScenarios);
