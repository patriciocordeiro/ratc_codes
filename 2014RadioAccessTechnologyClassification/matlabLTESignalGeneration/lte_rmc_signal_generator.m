%%
% created in 28/10/2014
% author Leonardo Ramalho
%%
clear;

%% config parameters

rmc_reference = 'R.4';
number_of_frames = 1;

file_output_name= 'inputR4';
file_bin_path = '';
%file_bin_path   = 'vector_quantization\';
%file_bin_path   = 'D:\LAPS_LASSE\svn\Lasse\compression\msvq\testsR5\';

save_signal_in_a_file =0;

%% prepare files names
file_output_name_mat        = [file_output_name];
file_output_name_bin_real   = [file_bin_path file_output_name '_real.dat'];
file_output_name_bin_imag   = [file_bin_path file_output_name '_imag.dat'];

%% 
%channel bandwidth options
lte_param.bw=[1.4 3 5 10 15 20];
lte_param.samples_per_slot = [960 1920 3840 7680 11520 15360];
% Number of resource block options
lte_param.Nrb=[6 15 25 50 75 100];

% The downlink RMC  are defined in Annex A.3 of TS36.101
rmc     = lteRMCDL(rmc_reference); % reference measurement channel
pdsch   = rmc.PDSCH;
bits_per_frame = sum(rmc.PDSCH.TrBlkSizes); % sum of all bits per subframe 
Nant    = rmc.CellRefP;     % number of antennas

option = find(lte_param.Nrb==rmc.NDLRB);

if(isempty(option))
    error('A invalid number of Resource blocks was selected');
end

samples_per_frame= 20*lte_param.samples_per_slot(option);

waveform_frames=zeros(samples_per_frame*number_of_frames,Nant);

for i=1:number_of_frames
    % generate random bits
    input_bits = randi([0 1], bits_per_frame, 1);
    % create 1 frame of a LTE RMC signal
    [waveform,rgrid,rmccFgOut] = lteRMCDLTool(rmc,input_bits);
    % concatenate frames
    waveform_frames((i-1)*samples_per_frame+1: i*samples_per_frame,:)=...
        waveform;
end

if(Nant>1)
    % waveform_frames is composed of 
    % real1_ant1 real1_ant2 real2_ant1 real2_ant2...
    % +
    % j(imag1_ant1 imag1_ant2 imag2_ant1 imag2_ant2...)
    waveform_frames=reshape(waveform_frames.',1,...
        Nant*samples_per_frame*number_of_frames);
end

%% save signal
if (1==save_signal_in_a_file)
    fp_real=fopen(file_output_name_bin_real,'wb');
    fp_imag=fopen(file_output_name_bin_imag,'wb');
    fwrite(fp_real,real(waveform_frames),'float');
    fwrite(fp_imag,imag(waveform_frames),'float');
    fclose(fp_real);
    fclose(fp_imag);
end
%%
% prints
disp('---------------------------------------------------------------');
rmc
pdsch