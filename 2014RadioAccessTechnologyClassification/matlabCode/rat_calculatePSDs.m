function S=rat_calculatePSDs(x,Nfft,Fs)
%convert time-domain complex envelopes to PSD
S=pwelch(x,hamming(Nfft),floor(Nfft/2),Nfft,Fs,'twosided'); %overlap is Nfft/2
