run('setSimulationVariables')

%read file
listOfcenvifFiles = [myOutputDir listOfIFComplexEnvelopes];
fp=fopen(listOfcenvifFiles,'r');
if fp < 0
    error(['Error reading ' listOfcenvifFiles]);
end
allFilesInList=textscan(fp,'%s'); %all goes to a unique cell!
fclose(fp);

for j=1:length(allFilesInList{1})
    inputFile=allFilesInList{1}{j};
    
    inputFile = strrep(inputFile,'\\',filesep);
    inputFile = strrep(inputFile,'//',filesep);
    inputFile = strrep(inputFile,'\',filesep);
    inputFile = strrep(inputFile,'/',filesep);
    
    x=read_complex_binary(inputFile);
    S=rat_calculatePSDs(x,Nfft,Fs);
    %change the extension
    outputFile=strrep(inputFile,['.' intermediateFreqEnvelopesFileExtension], ...
        ['.' intermediateFreqPSDsFileExtension]);
    write_real_binary(outputFile, S);
    disp(['Wrote file ' outputFile]);
end
