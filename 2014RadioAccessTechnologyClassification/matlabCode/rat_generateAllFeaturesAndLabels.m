run('setSimulationVariables')

for k=1:3 %train, test and validation
    thisFilesList = [myOutputDir listWithPSDAndAssociatedLabels{k}];
    fp=fopen(thisFilesList,'r');
    if fp < 0
        error(['Error reading ' thisFilesList]);
    end

    currentWekaFile = [myOutputDir outputWekaFile{k}];
    
    %% Write Weak file header
    fpOutput=fopen(currentWekaFile,'w');
    if fpOutput < 0
        error(['Error opening ' currentWekaFile]);
    end
    %% Write ARFF file header
    fprintf(fpOutput, '@relation %s\n', 'RATc');
    for i = 1:numFeatures{frontEndIndex}
        fprintf(fpOutput, '@attribute %s%d real\n', 'attribute', i);
    end
    fprintf(fpOutput, '@attribute class {');
    fprintf(fpOutput, '%s',['''' classLabels{1} '''']);
    for i=2:numClasses
        fprintf(fpOutput, ',');
        fprintf(fpOutput,'%s',['''' classLabels{i} '''']);
    end
    fprintf(fpOutput, '}\n');
    fprintf(fpOutput, '@data\n');
        
    allPairsInList=textscan(fp,'%s %s'); %all goes to a unique cell!
    fclose(fp);
    numFiles = length(allPairsInList{1});
    for i=1:numFiles
        psdifFileName = allPairsInList{1}{i};
        descriptionScript = allPairsInList{2}{i};
        run(descriptionScript);
        %descriptionScript provides information about:
        %centerFrequencies, labels and ratBandwidths
        %used for the respective cenvFileName
        psdS = read_real_binary(psdifFileName);
        
        disp(['Processing ' psdifFileName]);
        
        rat_extractFeaturesAndLabels(fpOutput,psdS,centerFrequencies,...
    labels,ratBandwidths);

        %rat_extractFeaturesAndLabels(fpOutput,S,centerFrequencies,...
        %    classLabels,labels,ratBandwidths,analysisBW,frequencyShift,Fs,...
        %    numFeatures)
    end
    fclose(fpOutput);
    disp(['Wrote ' currentWekaFile]);
end