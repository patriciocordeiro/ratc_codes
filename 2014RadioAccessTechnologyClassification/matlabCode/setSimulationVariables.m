%% First part: things you *must* change or at least check
% Specify your main directories
%NOTE: end all directories with filesep ('/' or '\' depending on
%if you are using Windows or Linux) otherwise concatenation is wrong

% Choose the user (a convenience)
%user='Patricio';
%user='Vitoria';
user='Aldebaro';
%Recall that directories need to end with filesep
switch user
    case 'Patricio' %Windows user
        myModuRatCDir = 'C:\svns\laps\telecom\2014RadioAccessTechnologyClassification\';
        pathToAldebarosCode = 'C:\svns\laps\latex\dslbook\'; %modify to match your system
    case 'Aldebaro' %Windows (argh) user
        myModuRatCDir = 'C:\svns\laps\telecom\2014RadioAccessTechnologyClassification\';
        pathToAldebarosCode = 'C:\svns\laps\latex\dslbook\'; %modify to match your system
    case 'Vitoria' %Linux user
        myModuRatCDir = '/home/v/2014RadioAccessTechnologyClassification/';
        pathToAldebarosCode = '/home/v/dslbook/'; %modify to match your system
end

%Specify directories:
myRATRootDir = [myModuRatCDir 'simulation_amqam' filesep];
myBasebandCenvTimeDomainDir = [myRATRootDir 'complexBasebandEnvTimeDomain' filesep];

%Specify all scenarios of this simulation
directoriesWithScenarios{1}='scenarioSimple';
directoriesWithScenarios{2}='scenario3Classes';

% Technical values
%Always include 'noRAT' as the last class even if you are not going
%to use it by shouldSkipNoRATExamples!
classLabels = {'AM','QAM','DSBSC','noRAT'};
%In case you do not want to include the "noRAT" in ARFF file, enable:
shouldSkipNoRATExamples = 1; %use 1 to enable or 0 otherwise

%minRFFreq = 100e3; %RF frequency range: min to max
%maxRFFreq = 200e3;
minIFFreq = 1e3; %IF frequency range: min to max
maxIFFreq = 99e3;
Fs = 500e3; %sampling frequency in Hz

%BWt = 200e6; %BW total
minBW = 5e3; %minimum BW in Hz
maxBW = 20e3; %maximum BW in Hz
analysisBW = 2e3; %for each band a labeled example is generated

minRATSeparationInHz = 5e3; %minimum separation between RATs, taking
%in account their center frequencies and bandwidth

numberOfPSDBlocks = 8; %number of blocks for Welch's
minNumberOfBinsWithinBW=24; %minimum FFT bins for a bandwidth

N0_dBmHz = -100; %unilateral PSD in dBm/Hz

% Front ends
allFrontEnds{1}='rat_frontendPSD'; %simply calculates PSD
allFrontEnds{2}='rat_frontendDCT'; %takes the DCT of the PSD
allFrontEnds{3}='rat_frontendBWAndOtherMeasurements'; %measures BW
numFeatures{1}=16; %number of features of each front end
numFeatures{2}=15;
numFeatures{3}=3;
frontEndIndex = 1; %index of chosen front end

% General parameters to control simulation
showPlots=0; %use 1 to show plots

% Design prototype filter
%h=butter %TO-DO

%% Second part: things you do not need to change

myOutputDir = [myRATRootDir 'output' filesep];

%define names for lists and files
nameListWithTrainScriptsPerScenario{1}='trainScriptsPerScenarioList.txt';
nameListWithTrainScriptsPerScenario{2}='testScriptsPerScenarioList.txt';
nameListWithTrainScriptsPerScenario{3}='validationScriptsPerScenarioList.txt';
listWithPSDAndAssociatedLabels{1}='trainPSDsAndScenariosList.txt';
listWithPSDAndAssociatedLabels{2}='testPSDsAndScenariosList.txt';
listWithPSDAndAssociatedLabels{3}='validationPSDsAndScenariosList.txt';
listOfIFComplexEnvelopes = 'filesToHavePSDCalculated.txt';
outputWekaFile{1}='amqam_train.arff';
outputWekaFile{2}='amqam_test.arff';
outputWekaFile{3}='amqam_validation.arff';

%% Extensions
basebandEnvelopesFileExtension = 'cenvbb';
intermediateFreqEnvelopesFileExtension = 'cenvif';
intermediateFreqPSDsFileExtension = 'psdif';

%% Third part: Pre-calculated and useful variables
if 1 %add folder with functions to path
    P=path;
    path(P,[pathToAldebarosCode 'Code' filesep 'MatlabOctaveFunctions']);
    addpath([myModuRatCDir 'matlabCode'],'-end');
end

df = minBW/minNumberOfBinsWithinBW; %FFT frequency spacing
M = ceil(Fs/df);
b=nextpow2(M);
Nfft=2^b; %choose a power of 2 for FFT-length
pwelchOverlap=floor(Nfft/2);

BW_IF = maxIFFreq - minIFFreq;
frequencyShift = analysisBW/2; %shift between examples in PSD domain

if analysisBW > minBW/2
    warning('analysisBW may be too large: analysisBW > minBW/2');
end

%S=pwelch(x,hamming(M),floor(M/2),Nfft,Fs,'twosided'); %overlap is M/2

numSamples = numberOfPSDBlocks*pwelchOverlap+Nfft;
%numSamples = Fs*totalDuration;

totalDuration=numSamples/Fs; %seconds

numClasses = length(classLabels);
if shouldSkipNoRATExamples==1
    numClasses = numClasses-1;
end

N0 = 1e-3 * 10^(0.1*N0_dBmHz); %N0 in Watts/Hz
noisePowerPerDimension = N0/2 * Fs;

if BW_IF >= Fs/2
    error('BW_IF >= Fs/2. Increase Fs!')
end

numScenarios=length(directoriesWithScenarios);
