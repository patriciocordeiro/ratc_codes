function rat_extractFeaturesAndLabels(fpOutput,psdS,centerFrequencies,...
    theseLabels,ratBandwidths)
%function rat_extractFeaturesAndLabels(fpOutput,psdS,centerFrequencies,...
%    theseLabels,ratBandwidths)
%Inputs:
%fpOutput - is a file pointer to write the Weka file
%psdS - is the PSD in IF
%centerFrequencies,theseLabels,ratBandwidths - identity the RATs in this PSD
%IMPORTANT: RAT is here considered to be completely within the analysis
%band. In other words, an "example" is labeled as GSM only if both its start
%and end indices are within the range of indices associated to the RAT.
%Example (with only one RAT in this PSD, which is GSM). Assume that
%analysisBWInTones = 10;
%ratStarts = [40];
%ratEnds = [150];
%assume that for a given "example":
%k=firstToneIndex:firstToneIndex+analysisBWInTones-1;
%generated
%k=[35 36 37 38 39 40 41 42];
%This example is NOT labeled as GSM (it will be labeled as noRAT)
%because 35 36 37 38 39 is not part of the RAT and
%if ratStarts(j)<=k(1) && ratEnds(j)>=k(end)
%will not be true. But if
%k=[40 41 42 43 44];
%then this example would be labeled as GSM. 
%This is simply a heuristic that can be changed.

run('setSimulationVariables')

if 0 %debugging
    clear all
    run('amqam_setSimulationVariables')
    
    labels=[2 1];
    centerFrequencies=[10e3 60e3];
    ratBandwidths=[5e3 8e3];
    analysisBW=2e3;
    frequencyShift=analysisBW;
    Fs=200e3;
    psd=1:2000;
end
%% Constants
%% Pre-compute values
numRats=length(centerFrequencies);
deltaF = Fs/Nfft;
analysisBWInTones = round(analysisBW/deltaF);
frequencyShiftInTones = round(frequencyShift/deltaF);
%get indices that indicate where a RAT starts and ends
ratStarts = round((centerFrequencies-ratBandwidths/2)/deltaF);
ratEnds = round((centerFrequencies+ratBandwidths/2)/deltaF);
%the convention is to use the frequencies from -Fs/2 to Fs/2
%so the indices here can be negative numbers. Correct that:
ratStarts = ratStarts + Nfft/2;
ratEnds = ratEnds + Nfft/2;
ratStarts(ratStarts<1)=1; %truncate
ratEnds(ratEnds>Nfft)=Nfft; %truncate

%the number of examples (feature vectors + class) that is contained
%in this PSD
numExamples = floor((Nfft-analysisBWInTones)/frequencyShiftInTones)+1;

%pre-allocate
allFeatures=zeros(numExamples,numFeatures{frontEndIndex});
%initialize with noRAT class index, always the last index.
%note that numClasses can be smaller than length(classLabels)
%but this will not impact
allLabels=length(classLabels)*ones(1,numExamples); 

firstToneIndex=1; %initialize
for i=1:numExamples;
    %update:
    k=firstToneIndex:firstToneIndex+analysisBWInTones-1;
    firstToneIndex=firstToneIndex+frequencyShiftInTones; %update for next iteration
    %% Extract features for each analysisBW    
    %all front ends have psdS and k as inputs
    commandLine = ['x=' allFrontEnds{frontEndIndex} '(psdS,k);'];
    eval(commandLine) %evaluate previous command line: run front end
    if length(x) ~= numFeatures{frontEndIndex}
        disp(['#### Front end calculated ' num2str(length(x)) ' features ####'])
        error('Number of features does not match numFeatures{frontEndIndex}')
    end
    allFeatures(i,:)=x; %store features of this example
    %% Find all labels
    for j=1:numRats
        if ratStarts(j)<=k(1) && ratEnds(j)>=k(end)
            %RAT is completely within analysis band
            allLabels(i)=theseLabels(j);
            break;
        end
    end
    %% Write example on output file pointer fpOutput (file was already open)
    if shouldSkipNoRATExamples == 1
        if allLabels(i) ~= length(classLabels)
            %write only if it is a RAT (it is not noRAT)
            writeToWekaARFF(fpOutput,allFeatures(i,:),classLabels{allLabels(i)})
            if showPlots == 1
                plot(allFeatures(i,:)); title(classLabels{allLabels(i)}); pause
            end            
        end
    else %write all examples
        writeToWekaARFF(fpOutput,allFeatures(i,:),classLabels{allLabels(i)})
    end
end
