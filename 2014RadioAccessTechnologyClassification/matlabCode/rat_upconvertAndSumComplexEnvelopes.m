function rat_upconvertAndSumComplexEnvelopes(complexEnvelopes,Fs,...
    centerFrequencies,outputFileName,numSamples,...
    noisePowerPerDimension,basebandEnvelopesFileExtension)

%complexEnvelopes{1}

%function rat_upconvertAndSumComplexEnvelopes(complexEnvelopes,Fs,...
%    centerFrequencies,inputFileName,numSamples)
numRats = length(complexEnvelopes);
if max(centerFrequencies) > Fs/2
    error('max(centerFrequencies) > Fs/2')
end
xallSignals=zeros(numSamples,1); %use column vectors
for i=1:numRats
    inputFileName = complexEnvelopes{i};
    inputFileName = strrep(inputFileName,'\\',filesep);
    inputFileName = strrep(inputFileName,'//',filesep);
    inputFileName = strrep(inputFileName,'\',filesep);
    inputFileName = strrep(inputFileName,'/',filesep);
    
    inputFileName = [inputFileName '.' basebandEnvelopesFileExtension];
    
    x=read_complex_binary(inputFileName);
    if 0
        pwelch(x); pause
    end
    if length(x) ~= numSamples
        disp(['Maybe you changed the front end and forgot to update ' ...
            'the number of files available?']);
        error(['File ' inputFileName ' has ' ...
            num2str(length(x)) ' samples. It should have ' ...
            num2str(numSamples) ' samples.'])
    end
    n=transpose(0:numSamples-1); %use column vectors
    carrier=exp(1j * 2*pi * (centerFrequencies(i)/Fs) * n);
    xallSignals = xallSignals +  carrier .* x;
    if 0
        pwelch(xallSignals); pause
    end
end

%% Add noise
noise = sqrt(noisePowerPerDimension)*(randn(size(xallSignals)) + ...
    1j*randn(size(xallSignals)));

write_complex_binary(outputFileName, xallSignals+noise);
if 0
    subplot(211), pwelch(xallSignals);
    subplot(212), pwelch(noise); pause
    pwelch(xallSignals+noise); pause
end
disp(['Wrote ' outputFileName]);
