run('setSimulationVariables')

%the list below lists all (train, test and validation) cenvif files
%that will be later converted to PSDs
listOfcenvifFiles = [myOutputDir listOfIFComplexEnvelopes];
fpCenvOut=fopen(listOfcenvifFiles,'w');
if fpCenvOut < 0
    error(['Error opening ' listOfcenvifFiles])
end

for k=1:3 %train, test and validation
    thisFilesList = [myOutputDir listWithPSDAndAssociatedLabels{k}];
    fpOutput=fopen(thisFilesList,'w');
    if fpOutput < 0
        error(['Error opening ' thisFilesList])
    end
    for i=1:numScenarios
        %setup variable for this scenario
        description=[myRATRootDir directoriesWithScenarios{i} ...
            filesep 'scenarioDescription.m'];
        run(description);
        
        %get file
        thisFilesList=[myRATRootDir directoriesWithScenarios{i} ...
            filesep nameListWithTrainScriptsPerScenario{k}];
        %read file
        fp=fopen(thisFilesList,'r');
        if fp < 0
            disp(['Error reading ' thisFilesList '.'])
            error('Does this file exist? If not, you have to create it!');
        end
        allFilesInList=textscan(fp,'%s'); %all goes to a unique cell!
        fclose(fp);
        for j=1:length(allFilesInList{1})
            complexEnvelopes=[]; %let it to be defined by thisScript
            thisScript=[myRATRootDir directoriesWithScenarios{i} ...
                filesep allFilesInList{1}{j}];
            run(thisScript); %this script will define: complexEnvelopes
            %and outputFileName
            
            %Check values
            if min(ratBandwidths) < minBW
                error([thisScript 'has too small BW'])
            end
            if max(ratBandwidths) > maxBW
                error([thisScript 'has too large BW'])
            end
            if min(centerFrequencies-ratBandwidths/2) < minIFFreq
                error([thisScript 'has too small centerFrequencies'])
            end
            if max(centerFrequencies+ratBandwidths/2) > maxIFFreq
                error([thisScript 'has too large centerFrequencies'])
            end
            %TO-DO: check minRATSeparationInHz
            
            %compose signal for that scenario
            outputFileName = strrep(outputFileName,'\\',filesep);
            outputFileName = strrep(outputFileName,'//',filesep);
            outputFileName = strrep(outputFileName,'\',filesep);
            outputFileName = strrep(outputFileName,'/',filesep);
            
            cenvOutputFileName = [outputFileName '.' intermediateFreqEnvelopesFileExtension];
            rat_upconvertAndSumComplexEnvelopes(complexEnvelopes,Fs,...
                centerFrequencies,cenvOutputFileName,numSamples,...
                noisePowerPerDimension,basebandEnvelopesFileExtension);
            
            psdifOutputFileName = [outputFileName '.' intermediateFreqPSDsFileExtension];
            fprintf(fpOutput,'%s %s\n',psdifOutputFileName,description);
            fprintf(fpCenvOut,'%s\n',cenvOutputFileName);
        end
    end
    fclose(fpOutput);
end
fclose(fpCenvOut);