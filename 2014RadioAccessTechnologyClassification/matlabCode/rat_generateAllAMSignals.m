run('setSimulationVariables')

N=4; %number of AM signals (or AM radio stations)
%The input files are named amFile1_Fs44100Hz.wav, amFile2_Fs44100Hz.wav, ...
inputPath=[myRATRootDir 'amSignals' filesep]; %directory where the files can be found, e.g. 'c:/mydir'
outputPath=myBasebandCenvTimeDomainDir; %output directory for files
%outputPathTest='./test'; %output directory for test files
%The first file determines the adopted sampling frequency Fs
inputFileName=[inputPath 'amFile1_Fs44100Hz.wav']; %name of first file
inputFileName
[x,Fsoriginal]=wavread(inputFileName); %Fs is the sampling frequency (Hz)
%soundsc(x,Fs); disp('Press a key'); pause
%% Do the magic
U=round(Fs/Fsoriginal); %upsampling factor (must be an integer) to increase Fsoriginal
%Fsoriginal=Fs/U; %adjust Rsym, if necessary
%S=ceil(numSamples/L); %number of symbols
M=ceil(numSamples/U); %number of samples assumed for the input signal

BWam = 5000; %each AM has BW=5 kHz and their freq. spacing is 10 kHz
h=fir1(50,BWam/Fsoriginal); %lowpass filter
y=zeros(numSamples,1); %pre-allocate space for output signal
n=transpose(0:numSamples-1);
modulationIndex=0.5; %number within range ]0, 1[
for i=1:N
    inputFileName=[inputPath 'amFile' num2str(i) '_Fs44100Hz.wav'];
    disp(['Processing ' inputFileName])
    [x,Fs_temp]=wavread(inputFileName);
    x=conv(x,h);
    x(1:floor(length(x)/2))=[]; %take out transient
    x=x(1:M); %force all signals to have the same duration (length)
    xup=resample(x,U,1);
    xMax=max(abs(xup));
    A=xMax/modulationIndex; %carrier amplitude
    y=A+xup;
    %plot(xrf,'r'); hold on; plot(xup);
    outputFileName=[outputPath '/amFs' num2str(Fs/1e3) ...
        'kHzBw' num2str(BWam/1e3) 'kHz_' num2str(i) '.' basebandEnvelopesFileExtension];
    write_complex_binary(outputFileName, y(1:numSamples));
    disp(['Wrote ' outputFileName]);
end
